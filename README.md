# PDF Cleaner
## A simple tool for selective text removing from multiple PDF files.
### Downloads
- [Windows executable](https://gitlab.com/marcinszelewicki/pdf-cleaner/-/raw/master/pdf-cleaner.exe)
(standalone, might raise warnings for unsigned exe)

- [Windows executable wrapper]()
(requires jar in same folder)

- [executable jar](https://gitlab.com/marcinszelewicki/pdf-cleaner/-/raw/master/pdf-cleaner.jar)
(standalone)


Project can also be built with Maven using `mvn clean install`


### How it works
1. PDF document is scanned and all tokens containing text are retrieved.
    > The order of those tokens should always be same for given document
2. Tokens indicated by user (by their index) are cleaned - replaced by `empty byte array`  
    > **⚠ note:** Original file is never altered


### Usage
1. Select one or more .pfd files (multi select with `ctrl` or `shift`)

2. Select range of indexes to clean - **indexed from 1**

    *input of `1,5-7,13` will result in cleaning indexes `1,5,6,7,13`*
                                       
    > **⚠ note:** Finding right indexes might require multiple tries,
    as they sometimes appear in slightly different order
    from the one visible when viewing the document.
                                       
    > Some documents will contain `string` tokens
    that are not visible when displaying document.
    In such documents it might be necessary to skip some indexes.

    *if index 1 does not remove the first visible text
    use index 2, 3 or higher*

3. Program generates result files in same folder as source,
with `_clean` suffix added to file name

    *cleaning `a.pdf` will create `a_clean.pdf` in the same folder*


## LICENSE
***This is a non-profit educational project.***

*All content used in this project including, but not limited to,
text, image, audio, video
is either self-made or used without permission, 
with all rights reserved to respective owners, 
and no challenge to their status intended.*

PDF Cleaner uses [Apache PDFBox](https://pdfbox.apache.org/)
Which is under appropriate [Apache License](https://www.apache.org/licenses/)

This software is under [WTFPL](http://www.wtfpl.net/txt/copying/)

[![screenshot](http://www.wtfpl.net/wp-content/uploads/2012/12/logo-220x1601.png)](http://www.wtfpl.net/)

<hr/>
author: kuduacz.pl
<hr/>
