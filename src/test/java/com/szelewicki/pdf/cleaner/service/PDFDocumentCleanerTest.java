package com.szelewicki.pdf.cleaner.service;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PDFDocumentCleanerTest {

    private final PDFDocumentCleaner underTest = new PDFDocumentCleaner();

    @Test
    public void shouldClean(@TempDir Path tempDir) throws IOException {
        //Given
        Path file = Paths.get(getClass().getResource("/test.pdf").getPath());
        Path source = tempDir.resolve(file.getFileName());
        Path destination = tempDir.resolve("result.pdf");
        Files.copy(file, source);

        //When
        underTest.clean(source.toFile(), destination.toFile(), Arrays.asList(2, 4, 7, 8));

        //Then
        PDFTextStripper stripper = new PDFTextStripper();
        PDDocument originalDoc = PDDocument.load(file.toFile());
        String originalText = stripper.getText(originalDoc);
        PDDocument cleanedDoc = PDDocument.load(destination.toFile());
        String cleanedText = stripper.getText(cleanedDoc);
        String expectedOriginal = getExpected(IntStream.rangeClosed(1, 8));
        String expectedCleaned = getExpected(IntStream.of(1, 3, 5, 6));

        assertEquals(expectedOriginal, originalText);
        assertEquals(expectedCleaned, cleanedText);
        assertEquals(originalDoc.getNumberOfPages(), 2);
        assertEquals(cleanedDoc.getNumberOfPages(), 2);

        originalDoc.close();
        cleanedDoc.close();
    }

    private String getExpected(IntStream intStream) {
        return intStream.mapToObj(i -> "text" + i)
                .reduce((a, b) -> a + System.lineSeparator() + b)
                .orElseThrow(RuntimeException::new) + System.lineSeparator();
    }
}
