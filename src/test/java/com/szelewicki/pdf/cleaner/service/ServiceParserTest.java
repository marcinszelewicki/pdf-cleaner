package com.szelewicki.pdf.cleaner.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.nio.file.Path;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ServiceParserTest {

    private final ServiceParser underTest = new ServiceParser();

    @Test
    public void shouldParseIndexes_withWhitespace_andTrailingComma() {
        //When
        Collection<Integer> result = underTest.parseIndexes(" 1,3- 5 ,6 , ");

        //Then
        assertEquals(Stream.of(1, 3, 4, 5, 6).collect(Collectors.toSet()), result);
    }

    @Test
    public void shouldParseDestinationPath(@TempDir Path tempDir) {
        //When
        Path result = underTest.parseDestinationPath(tempDir.resolve("abc.pdf"), "_clean");

        //Then
        assertEquals(tempDir.resolve("abc_clean.pdf"), result);
    }
}
