package com.szelewicki.pdf.cleaner.component;

import com.szelewicki.pdf.cleaner.config.AppConfig;
import com.szelewicki.pdf.cleaner.config.AppTranslations;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.File;
import java.net.URL;
import java.util.Optional;

public class Components {

    private final JFrame frame;
    private final FileNameExtensionFilter FILE_FILTER = new FileNameExtensionFilter("PDF Files", AppConfig.PDF);
    private File LAST_PATH = AppConfig.ROOT_PATH.toFile();

    public Components() {
        frame = new JFrame();
        frame.setTitle(AppTranslations.APP_TITLE);
        URL resource = Components.class.getResource(AppConfig.ICO_PATH);
        ImageIcon imageIcon = new ImageIcon(resource);
        frame.setIconImage(imageIcon.getImage());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setAlwaysOnTop(true);
        frame.setLocationRelativeTo(null);
        frame.setUndecorated(true);
        JPanel container = new JPanel();
        JTextArea textArea = new JTextArea(AppTranslations.APP_BACKGROUND_TEXT);
        textArea.setEditable(false);
        textArea.setFont(Font.decode(Font.MONOSPACED));
        container.add(textArea);
        frame.setContentPane(container);
        frame.setSize(1, 1);
        frame.pack();
        frame.setVisible(true);
    }

    public File[] getFilesFromUser() {
        JFileChooser jFileChooser = new JFileChooser();
        jFileChooser.setDialogTitle(AppTranslations.CHOOSER_TITLE);
        jFileChooser.setCurrentDirectory(LAST_PATH);
        jFileChooser.setMultiSelectionEnabled(true);
        jFileChooser.setFileFilter(FILE_FILTER);
        int i = jFileChooser.showOpenDialog(frame);
        File[] selectedFiles = jFileChooser.getSelectedFiles();
        if (selectedFiles != null && selectedFiles.length > 0) {
            LAST_PATH = selectedFiles[0].toPath().getParent().toFile();
            return selectedFiles;
        }
        return new File[]{};
    }

    public Object getIndexesFromUser() {
        return JOptionPane.showInputDialog(frame,
                AppTranslations.INDEXES_INPUT_TEXT,
                AppTranslations.INDEXES_INPUT_TITLE,
                JOptionPane.PLAIN_MESSAGE, null, null,
                AppConfig.DEFAULT_INDEXES);
    }

    /**
     * @return 0 if user selected YES, 1 if NO, -1 if window was closed
     */
    public int showSummary(String message) {
        return JOptionPane.showConfirmDialog(frame,
                message, AppTranslations.SUMMARY_TITLE,
                JOptionPane.YES_NO_OPTION,
                JOptionPane.INFORMATION_MESSAGE,
                null);
    }

    public String getSuffixFromUser() {
        Object userInput = JOptionPane.showInputDialog(frame,
                AppTranslations.SUFFIX_INPUT_TEXT,
                AppTranslations.SUFFIX_INPUT_TITLE,
                JOptionPane.PLAIN_MESSAGE, null, null,
                AppConfig.DEFAULT_PROCESSED_FILE_SUFFIX);
        return Optional.ofNullable(userInput)
                .map(String::valueOf)
                .orElse(AppConfig.DEFAULT_PROCESSED_FILE_SUFFIX);
    }
}
