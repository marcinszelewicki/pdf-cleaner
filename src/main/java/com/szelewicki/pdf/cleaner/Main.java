package com.szelewicki.pdf.cleaner;

import com.szelewicki.pdf.cleaner.component.Components;
import com.szelewicki.pdf.cleaner.service.PDFDocumentCleaner;
import com.szelewicki.pdf.cleaner.service.ServiceParser;

/**
 ## LICENSE
 ***This is a non-profit educational project.***

 *All content used in this project including, but not limited to,
 text, image, audio, video
 is either self-made or used without permission,
 with all rights reserved to respective owners,
 and no challenge to their status intended.*

 PDF Cleaner uses [Apache PDFBox](https://pdfbox.apache.org/)
 Which is under appropriate [Apache License](https://www.apache.org/licenses/)

 This software is under [WTFPL](http://www.wtfpl.net/txt/copying/)

 <hr/>
 @author kuduacz.pl
 <hr/>
 */
public class Main {
    public static void main(String[] args) {
        new Controller(new Components(), new ServiceParser(), new PDFDocumentCleaner())
                .run();
    }
}
