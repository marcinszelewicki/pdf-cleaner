package com.szelewicki.pdf.cleaner;

import com.szelewicki.pdf.cleaner.component.Components;
import com.szelewicki.pdf.cleaner.config.AppTranslations;
import com.szelewicki.pdf.cleaner.service.PDFDocumentCleaner;
import com.szelewicki.pdf.cleaner.service.ServiceParser;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;

public class Controller {

    private final Components components;
    private final ServiceParser serviceParser;
    private final PDFDocumentCleaner documentCleaner;

    public Controller(Components components,
                      ServiceParser serviceParser,
                      PDFDocumentCleaner documentCleaner) {
        this.components = components;
        this.serviceParser = serviceParser;
        this.documentCleaner = documentCleaner;
    }

    public void run() {
        File[] files = components.getFilesFromUser();
        if (files.length == 0) exit(AppTranslations.NO_FILES_MESSAGE);

        Object indexesFromUser = components.getIndexesFromUser();
        Collection<Integer> indexes = serviceParser.parseIndexes(indexesFromUser);
        if (indexes.isEmpty()) exit(AppTranslations.NO_INDEXES_MESSAGE);

        String fileSuffix = components.getSuffixFromUser();
        int ok = 0, err = 0;
        for (File f : files) {
            Path source = Paths.get(f.getPath());
            Path destination = serviceParser.parseDestinationPath(source, fileSuffix);
            boolean success = documentCleaner.clean(source.toFile(), destination.toFile(), indexes);
            if (success) ok++;
            else err++;
        }
        exit(getSummary(files, ok, err));
    }

    private void exit(String message) {
        int i = components.showSummary(message);
        if (i == 0) run();
        else System.exit(0);
    }

    private String getSummary(File[] files, int ok, int err) {
        return String.format(AppTranslations.COMPLETED_MESSAGE_FORMAT,
                ok,
                ok > 1 ? "s" : "",
                files[0].toPath().getParent(),
                err > 0 ? err : "No",
                err == 1 ? "" : "s");
    }
}
