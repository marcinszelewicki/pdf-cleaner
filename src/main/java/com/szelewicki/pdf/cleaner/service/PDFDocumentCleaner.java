package com.szelewicki.pdf.cleaner.service;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.pdfparser.PDFStreamParser;
import org.apache.pdfbox.pdfwriter.ContentStreamWriter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDStream;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.List;

public class PDFDocumentCleaner {

    public boolean clean(File source, File dest, Collection<Integer> indexes) {
        try {
            PDDocument document = replaceText(source, indexes);
            document.save(dest);
            document.close();
            return true;
        } catch (Exception e) {
            System.err.println(source.getName() + " " + e.toString());
            return false;
        }
    }

    private PDDocument replaceText(File source, Collection<Integer> cosIndexes) throws IOException {

        PDDocument document = PDDocument.load(source);

        int cosCounter = 1;
        int numberOfPages = document.getNumberOfPages();
        for (int pageIndex = 0; pageIndex < numberOfPages; pageIndex++) {

            PDPage page = document.getPage(pageIndex);
            List<?> tokens = getPageTokens(page);

            for (Object o : tokens) {
                if (o instanceof COSString) {
                    COSString cosString = (COSString) o;
                    if (cosIndexes.contains(cosCounter)) {
                        cosString.setValue(new byte[]{});
                    }
                    cosCounter++;
                }
            }

            updatePageContents(document, page, tokens);
        }

        return document;
    }

    private List<?> getPageTokens(PDPage page) throws IOException {
        PDFStreamParser parser = new PDFStreamParser(page);
        parser.parse();
        return parser.getTokens();
    }

    private void updatePageContents(PDDocument document, PDPage page, List<?> tokens) throws IOException {
        PDStream updatedStream = new PDStream(document);
        OutputStream out = updatedStream.createOutputStream(COSName.FLATE_DECODE);
        ContentStreamWriter tokenWriter = new ContentStreamWriter(out);
        tokenWriter.writeTokens(tokens);
        out.close();
        page.setContents(updatedStream);
    }
}
