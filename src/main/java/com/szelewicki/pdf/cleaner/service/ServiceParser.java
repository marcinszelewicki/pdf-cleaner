package com.szelewicki.pdf.cleaner.service;

import com.szelewicki.pdf.cleaner.config.AppConfig;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ServiceParser {

    public Path parseDestinationPath(Path source, String fileSuffix) {
        String destinationFileName = source.getFileName().toString()
                .replaceAll(String.format("\\%s", AppConfig.PDF_EXTENSION), "")
                .concat(fileSuffix)
                .concat(AppConfig.PDF_EXTENSION);
        return source.getParent().resolve(destinationFileName);
    }

    public Collection<Integer> parseIndexes(Object userInput) {
        return Optional.ofNullable(userInput)
                .map(String::valueOf)
                .map(this::parseIndexesInput)
                .orElse(Collections.emptySet());
    }

    private Collection<Integer> parseIndexesInput(String input) {
        try {
            return Arrays.stream(input.split(","))
                    .map(String::trim)
                    .filter(s -> !s.isEmpty())
                    .flatMap(this::parseRange)
                    .collect(Collectors.toSet());
        } catch (Exception e) {
            return Collections.emptySet();
        }
    }

    private Stream<Integer> parseRange(String input) {
        if (input.contains("-")) {
            String[] split = input.split("-");
            return IntStream.rangeClosed(Integer.parseInt(split[0].trim()), Integer.parseInt(split[1].trim()))
                    .boxed();
        }
        return Stream.of(Integer.parseInt(input));
    }
}
