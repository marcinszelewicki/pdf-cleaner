package com.szelewicki.pdf.cleaner.config;

public interface AppTranslations {
    String SEPARATOR = System.lineSeparator();
    String APP_TITLE = "PDF Cleaner";
    String CHOOSER_TITLE = APP_TITLE + " - select files";
    String INDEXES_INPUT_TITLE = APP_TITLE + " - select fields";
    String SUFFIX_INPUT_TITLE = APP_TITLE + " - result file suffix";
    String SUMMARY_TITLE = APP_TITLE + " - summary";
    String INDEXES_INPUT_TEXT = "Select fields to clear" + SEPARATOR + "(e.g. 1,2,5-9,13)";
    String SUFFIX_INPUT_TEXT = "Select file suffix" + SEPARATOR + "If suffix is e.g. '_clean'" + SEPARATOR +
            "file 'a.pdf' will be saved as 'a_clean.pdf'" + SEPARATOR + SEPARATOR +
            "If 'a_clean.pdf' already exists" + SEPARATOR +
            "it will be overwritten without warning." + SEPARATOR;
    String CLEAN_AGAIN = SEPARATOR + "Clean again?";
    String COMPLETED_MESSAGE_FORMAT = "COMPLETED" + SEPARATOR + SEPARATOR +
            "Successfully cleaned %d file%s in" + SEPARATOR +
            "%s" + SEPARATOR + SEPARATOR +
            "%s error%s" + SEPARATOR + CLEAN_AGAIN;
    String COMPLETED_NOTHING_MESSAGE = "I have successfully done nothing ;)" + SEPARATOR + CLEAN_AGAIN;
    String NO_FILES_MESSAGE = "No files selected, so.." + SEPARATOR + SEPARATOR + COMPLETED_NOTHING_MESSAGE;
    String NO_INDEXES_MESSAGE = "No indexes were selected" + SEPARATOR +
            "or I could not read them, so.." + SEPARATOR + SEPARATOR + COMPLETED_NOTHING_MESSAGE;
    String APP_BACKGROUND_TEXT =
            "  __  _  __" + SEPARATOR +
            " | _|| \\| _" + SEPARATOR +
            " ||  |_/|| Cleaner " + SEPARATOR +
            SEPARATOR + "1.0 kuduacz.pl 2020";
}
