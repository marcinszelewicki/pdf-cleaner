package com.szelewicki.pdf.cleaner.config;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public interface AppConfig {

    Path ROOT_PATH = Paths.get(new File(".").getAbsolutePath());
    String DEFAULT_INDEXES = "5-9";
    String ICO_PATH = "/ico.png";
    String DEFAULT_PROCESSED_FILE_SUFFIX = "_clean";
    String PDF = "pdf";
    String PDF_EXTENSION = "." + PDF;
}
